'use strict';
import React, {
  Text,
  Component,
  TouchableOpacity,
  View
} from 'react-native';

export default class SimpleButton extends Component {
  render() {
       return(
        <TouchableOpacity onPress={this.props.onPress}>
           <View style={this.props.style}>
              <Text style ={this.props.textStyle}>
              {this.props.customText || 'Simple Button'}
              </Text>
           </View>
        </TouchableOpacity>
       );
  }
}
//define properties
SimpleButton.propTypes = {
  onPress: React.PropTypes.func.isRequired,
  customText: React.PropTypes.string,
  style: View.propTypes.style,
  textStyle: Text.propTypes.style
};
