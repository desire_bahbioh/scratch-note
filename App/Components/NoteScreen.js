/*@flow*/
'use strict';
import React, {
  StyleSheet,
  TextInput,
  Component,
  View
} from 'react-native';


export default class NoteScreen extends Component {
   render(){
       return(
        <View style={styles.container}>
           <TextInput ref="title" autoFocus={true}
                    autoCapitalize='sentences'
                    placeholder='Untitled' style={[styles.title, styles.marginSpacing]}
                        onEndEditing={(text)=>{this.refs.body.focus()}}
                        value={this.props.title}
                        />
                <TextInput ref="body" multiline={true}
                placeholder="Start typing"
                style={[styles.body, styles.marginSpacing]}
                value={this.props.body}
                />
             </View>
       );
   }
}

const styles = StyleSheet.create({
     container: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 64
     },
     title: {
       height: 40
     },
     body:{
       flex: 1
     },
     marginSpacing: {
         marginLeft: 20,
         marginRight: 20
     }
});
