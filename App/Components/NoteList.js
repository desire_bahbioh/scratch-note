'use strict'
import React,{
  Text,
  Component,
  StyleSheet,
  View,
  ListView,
  TouchableOpacity
} from 'react-native';

export default class NoteList extends Component {

    constructor (props){
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1!==r2});
    }

     render() {
          return (
            <ListView
              dataSource={
                  this.ds.cloneWithRows([
                      {title:"Note 1", body:"Body 1", id:1},
                      {title:"Note 2", body:"Body 2", id:2},
                      {title:"Note 3", body:"Body 3", id:3}
                  ])
              }
              renderRow={(rowData) => {
                  return(
                    <TouchableOpacity onPress={() => this._onPress(rowData)}>
                      <View style={styles.row}>
                        <Text>{rowData.title}</Text>
                      </View>
                    </TouchableOpacity>
                      )}}
      renderSeparator={(sectionID, rowID) => <View key={`${sectionID}-${rowID}`}
      style={styles.separator} />}
                     />
                    )
    }

    _onPress(rowData){
      this.props.navigator.push({
        name: 'createNote',
        note:{
          id: rowData.id,
          title: rowData.title,
          body: rowData.body
        }
      })
    }

}
const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC'
  },
  row: {
    flexDirection: 'row',
    padding: 12,
    height: 44
  }
})
