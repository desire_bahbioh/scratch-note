/*@flow*/
 /* Sample React Native App
 * https://github.com/facebook/react-native
 */
//1.import libraries, custom components or some code
'use strict';
import React, {
  AppRegistry,
  Navigator,
  Component,
  StyleSheet,
  Text,
  StatusBar,
  View
} from 'react-native';


import SimpleButton from './App/Components/SimpleButton.js';
import NoteScreen from './App/Components/NoteScreen.js';
import HomeScreen from './App/Components/HomeScreen.js';

var NavigationBarRouteMapper = {
  LeftButton: function(route, navigator, index, navState){
    switch(route.name){
      case 'createNote':
        return(
          <SimpleButton
           onPress={() => navigator.pop()}
           customText='Back'
           style={styles.navBarLeftButton}
           textStyle={styles.navBarButtonText}
           />
        );
      default:
        return null;
    }
  },
  RightButton: function(route, navigator, index, navState){
    switch(route.name){
      case 'home':
        return(
           <SimpleButton
            onPress={() => {navigator.push({
               name:'createNote'
            });
          }}
           customText='Create Note'
           style={styles.navBarRightButton}
           textStyle={styles.navBarButtonText}
         />
      );
      default:
         return null;
    }
  },
  Title: function(route, navigator, index, navState){
    switch(route.name){
      case 'home':
         return(
           <Text style={styles.navBarTitleText}>Scratch Notes</Text>
         );
      case 'createNote':
         return(
           <Text style={styles.navBarTitleText}>Scratch Notes</Text>
         );
    }
  }
};

//2.create components
class ScratchNote extends Component {
  constructor(props){
      super(props);
      StatusBar.setBarStyle('light-content', true);
  }
  renderScene(route, navigator) {
     switch (route.name) {
     //homescreen page
       case 'home':
             return(<HomeScreen navigator={navigator}/> );
     // create a note screen
        case 'createNote':
              return(<NoteScreen/>);
     }
   }

  render() {
    //navigation bar
    return (
      <Navigator
         initialRoute={{name:'home'}}
         renderScene={this.renderScene}
         navigationBar={
            <Navigator.NavigationBar
              routeMapper={NavigationBarRouteMapper}
              style={styles.navBar}
            />
         }
      />
    );
  }
}
//3.create stylesheets
const styles = StyleSheet.create({
  navContainer:{
    flex: 1
  },
  navBar:{
    backgroundColor:'#5B29C1',
    borderBottomColor:'#48209A',
    borderBottomWidth: 1
  },
  navBarTitleText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: 9 //iOS
    //marginVertical: 16, //android
  },
  navBarLeftButton: {
    paddingLeft: 10
  },
  navBarRightButton: {
    paddingRight: 10
  },
  navBarButtonText: {
    color: '#EEE',
    fontSize: 16,
    marginVertical: 10 //iOS
    //marginVertical: 16 //android
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  }
});
//4.display components on the screen
AppRegistry.registerComponent('ScratchNote', () => ScratchNote);
